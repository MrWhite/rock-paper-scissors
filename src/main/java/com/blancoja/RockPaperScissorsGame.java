package com.blancoja;

public class RockPaperScissorsGame extends Game{


    public RockPaperScissorsGame(int gameRounds) {
        super(gameRounds);
    }

    @Override
    public Result playRound(PlayersMoves moves) {
        Result resultActualIteration = new Result();
        if(moves.secondPlayer == moves.firstPlayer){
            resultActualIteration.value = "Draw";
        }
        if(moves.firstPlayer== App.play.ROCK&& moves.secondPlayer== App.play.SCISSORS ||
            moves.firstPlayer== App.play.SCISSORS&& moves.secondPlayer== App.play.PAPER ||
            moves.firstPlayer== App.play.PAPER&& moves.secondPlayer== App.play.ROCK){
            resultActualIteration.value = "1st Player";
            totalResults.firstPlayer += 1;
        }
        if(moves.secondPlayer== App.play.ROCK&& moves.firstPlayer== App.play.SCISSORS ||
            moves.secondPlayer== App.play.SCISSORS&& moves.firstPlayer== App.play.PAPER ||
            moves.secondPlayer== App.play.PAPER&& moves.firstPlayer== App.play.ROCK){
            resultActualIteration.value = "2nd Player";
            totalResults.secondPlayer += 1;
        }
        totalResults.actualGameRound += 1;
        return resultActualIteration;

    }
}
