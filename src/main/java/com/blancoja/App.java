package com.blancoja;

import java.util.Scanner;
import java.util.concurrent.TimeUnit;
import static spark.Spark.*;

@SuppressWarnings("ALL")
public class App
{
    public static final int ITERATIONS = 10;


    public enum play {
        ROCK,
        PAPER,
        SCISSORS
    }
    public static String filename = "the-file-name.txt";

    static IRandomMoveGenerator randomMoveGenerator= new RandomMove();

    public static void main( String[] args ) throws InterruptedException {
        Scanner myObj = new Scanner(System.in);
        Game rockPaperScissors = new RockPaperScissorsGame(ITERATIONS);
        System.out.println("-----------------------------------------------------------");
        System.out.println("-----------------------------------------------------------");
        System.out.println(" ROCK, PAPER & SCISSORS GAME ");
        System.out.println("-----------------------------");
        while(rockPaperScissors.output == null) {

            System.out.print("output destination(console / file):");
            String output = myObj.nextLine();
            if (output.equals("file")) {
                rockPaperScissors.setOutput(new FileOutput(filename));
            } else if (output.equals("console")) {
                rockPaperScissors.setOutput((new ConsoleOutput()));
            } else {
                System.out.println("No valid option choosed, please try again...");
            }
        }
        rockPaperScissors.output.startNewGame();

        roundLoop: while(ITERATIONS>rockPaperScissors.totalResults.actualGameRound){
            System.out.println("-----------------------------------------------------------");
            System.out.println("Round: " + (rockPaperScissors.totalResults.actualGameRound+1));
            System.out.print("choose Mode(fair/unfair(or remote)):");
            String gameMode = myObj.nextLine();
            switch (gameMode) {
                case "fair":
                    rockPaperScissors.setMode(new FairMode(randomMoveGenerator));
                    break;
                case "unfair":
                    rockPaperScissors.setMode(new UnfairMode(randomMoveGenerator));
                    break;
                case "remote":
                    rockPaperScissors.setMode(new RemoteMode(randomMoveGenerator));
                    break ;
                default :
                    System.out.println("No valid option choosed, please try again...");
                    continue roundLoop;

            }

            if(rockPaperScissors.mode instanceof RemoteMode){

                port(8080);
                org.eclipse.jetty.util.log.Log.setLog(new NoLogging());

                get("/play", (req, res) -> {
                    ((RemoteMode) rockPaperScissors.mode).resetRemoteLoop();
                    return "Game Played!!";
                });
                Thread.sleep(500);
                System.out.println("Call on browser : http://0.0.0.0:8080/play ");
                while(((RemoteMode) rockPaperScissors.mode).remoteLoop) {
                    Thread.sleep(500);
                }
                stop();
                Thread.sleep(500);
            }
            PlayersMoves moves=  rockPaperScissors.mode.getPlayersMoves();
            Result roundResult = rockPaperScissors.playRound(moves);

            rockPaperScissors.output.publishRoundResults(moves, roundResult, rockPaperScissors.totalResults.actualGameRound);
        }

        rockPaperScissors.output.publishFinalResults(rockPaperScissors.totalResults);


        System.exit(0);

    }

}
