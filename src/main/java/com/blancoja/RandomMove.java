package com.blancoja;

import java.util.concurrent.ThreadLocalRandom;

public class RandomMove implements IRandomMoveGenerator{


    @Override
    public App.play GetRandomMove() {
        return App.play.values()[ThreadLocalRandom.current().nextInt(0, 3)];
    }
}
