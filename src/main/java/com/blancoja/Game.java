package com.blancoja;

public abstract class Game {
    GameMode mode;
    GameOutput output;
    int gameRounds;
    TotalResults totalResults;

    public Game(int gameRounds) {
        this.gameRounds = gameRounds;
        totalResults= new TotalResults();
    }

    public Result playRound(PlayersMoves moves) {
        return null;
    }

    public void setMode(GameMode mode) {
        this.mode = mode;
    }

    public void setOutput(GameOutput output) {
        this.output = output;
    }
}
