package com.blancoja;



public class FairMode implements GameMode {

    IRandomMoveGenerator moveGenerator;

    public FairMode(IRandomMoveGenerator newMoveGenerator) {
        moveGenerator = newMoveGenerator;
    }

    @Override
    public PlayersMoves getPlayersMoves() {
        PlayersMoves playersMoves = new PlayersMoves();
        playersMoves.firstPlayer= moveGenerator.GetRandomMove();
        playersMoves.secondPlayer= moveGenerator.GetRandomMove();
        return playersMoves;
    }
}
