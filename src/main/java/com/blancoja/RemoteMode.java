package com.blancoja;

import java.util.concurrent.ThreadLocalRandom;

public class RemoteMode implements GameMode{
    boolean remoteLoop = true;

    IRandomMoveGenerator moveGenerator;

    public RemoteMode(IRandomMoveGenerator newMoveGenerator) {
        moveGenerator = newMoveGenerator;
    }


    @Override
    public PlayersMoves getPlayersMoves() {
        PlayersMoves playersMoves = new PlayersMoves();
        playersMoves.firstPlayer= moveGenerator.GetRandomMove();
        playersMoves.secondPlayer= moveGenerator.GetRandomMove();
        return playersMoves;
    }

    public void resetRemoteLoop() {
        remoteLoop = false;
    }
    public void setRemoteLoop() {
        remoteLoop = true;
    }
}
