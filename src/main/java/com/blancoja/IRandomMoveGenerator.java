package com.blancoja;

public interface IRandomMoveGenerator {
    App.play GetRandomMove();
}
