package com.blancoja;

public class ConsoleOutput implements GameOutput{


    @Override
    public void startNewGame() {
        System.out.println("-----------------------------------------------------------");

    }

    @Override
    public void publishRoundResults(PlayersMoves moves, Result result, int actualRound) {
        System.out.println("1st Player Choice:" + moves.firstPlayer);
        System.out.println("2nd Player Choice:" + moves.secondPlayer);
        System.out.println("Winner: " + result.value);
    }

    @Override
    public void publishFinalResults(TotalResults totalResults) {
        System.out.println("-----------------------------------------------------------");
        System.out.println("-----------------------------------------------------------");
        System.out.println("Final Score: ");
        System.out.println("-----------------");
        System.out.println("1st Player Score: " + totalResults.firstPlayer);
        System.out.println("2nd Player Score: " + totalResults.secondPlayer);
        System.out.println("-----------------------------------------------------------");
        System.out.println("-----------------------------------------------------------");
    }


}
