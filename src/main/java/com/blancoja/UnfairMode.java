package com.blancoja;

import java.util.concurrent.ThreadLocalRandom;

public class UnfairMode implements GameMode{

    IRandomMoveGenerator moveGenerator;

    public UnfairMode(IRandomMoveGenerator newMoveGenerator) {
        moveGenerator = newMoveGenerator;
    }

    @Override
    public PlayersMoves getPlayersMoves() {
        PlayersMoves playersMoves = new PlayersMoves();
        playersMoves.firstPlayer= moveGenerator.GetRandomMove();
        playersMoves.secondPlayer= App.play.ROCK;
        return playersMoves;
    }
}
