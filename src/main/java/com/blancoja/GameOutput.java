package com.blancoja;

public interface GameOutput {


     void startNewGame();
     void publishRoundResults(PlayersMoves moves, Result result, int actualRound);


     void publishFinalResults(TotalResults totalResults);
}
