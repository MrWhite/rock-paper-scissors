package com.blancoja;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

public class FileOutput implements GameOutput{
    PrintWriter writer;

    @Override
    public void startNewGame() {
        writer.println(" ROCK, PAPER & SCISSORS GAME ");
        writer.println("-----------------------------");
        writer.flush();
    }

    @Override
    public void publishRoundResults(PlayersMoves moves, Result result, int actualRound) {
        writer.println("Round " + actualRound + ": ");
        writer.println("-----------------");
        writer.println("1st Player Choice:" + moves.firstPlayer);
        writer.println("2nd Player Choice:" + moves.secondPlayer);
        writer.println("Winner: " + result.value);
        writer.println("-----------------------------------------------------------");
        writer.flush();
    }

    @Override
    public void publishFinalResults(TotalResults totalResults) {
        writer.println("-----------------------------------------------------------");
        writer.println("-----------------------------------------------------------");
        writer.println("Final Score: ");
        writer.println("-----------------");
        writer.println("1st Player Score: " + totalResults.firstPlayer);
        writer.println("2nd Player Score: " + totalResults.secondPlayer);
        writer.println("-----------------------------------------------------------");
        writer.println("-----------------------------------------------------------");
        writer.flush();
        writer.close();
    }


    public FileOutput(String filename) {

        try {
            FileWriter fileWriter = new FileWriter(filename, true);
            writer = new PrintWriter(fileWriter);

        } catch(IOException e) {
            System.out.println("Unable to create/open file: " + e.getMessage());
        }
    }
}
