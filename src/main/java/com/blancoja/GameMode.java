package com.blancoja;

public interface GameMode {
    PlayersMoves getPlayersMoves();
}
