package com.blancoja;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class UnfairModeTest {
    UnfairMode unfairMode;
    IRandomMoveGenerator fakeRandom = new MockRandomGenerator(App.play.ROCK);

    @BeforeEach
    public void setUp() {
        unfairMode = new UnfairMode(fakeRandom);
    }

    @Test
    public void checkGeneratedMovesAreRocks(){
        if(fakeRandom instanceof MockRandomGenerator) {
            ((MockRandomGenerator) fakeRandom).set_fakeMove(App.play.ROCK);
        }
        PlayersMoves moves = unfairMode.getPlayersMoves();
        assert(moves.firstPlayer.equals(App.play.ROCK) && moves.secondPlayer.equals(App.play.ROCK));
    }

    @Test
    public void checkGeneratedMovesArePapers(){
        if(fakeRandom instanceof MockRandomGenerator) {
            ((MockRandomGenerator) fakeRandom).set_fakeMove(App.play.PAPER);
        }
        PlayersMoves moves = unfairMode.getPlayersMoves();

        assert(moves.firstPlayer.equals(App.play.PAPER) && moves.secondPlayer.equals(App.play.ROCK));
    }

    @Test
    public void checkGeneratedMovesAreScissor(){
        if(fakeRandom instanceof MockRandomGenerator) {
            ((MockRandomGenerator) fakeRandom).set_fakeMove(App.play.SCISSORS);
        }
        PlayersMoves moves = unfairMode.getPlayersMoves();
        assert(moves.firstPlayer.equals(App.play.SCISSORS) && moves.secondPlayer.equals(App.play.ROCK));
    }
}
