package com.blancoja;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class RemoteModeTest {
    RemoteMode remoteMode;
    IRandomMoveGenerator fakeRandom = new MockRandomGenerator(App.play.ROCK);

    @BeforeEach
    public void setUp() {
        remoteMode = new RemoteMode(fakeRandom);
    }

    @Test
    public void checkGeneratedMovesAreRocks(){
        if(fakeRandom instanceof MockRandomGenerator) {
            ((MockRandomGenerator) fakeRandom).set_fakeMove(App.play.ROCK);
        }
        PlayersMoves moves = remoteMode.getPlayersMoves();
        assert(moves.firstPlayer.equals(App.play.ROCK) && moves.secondPlayer.equals(App.play.ROCK));
    }

    @Test
    public void checkGeneratedMovesArePapers(){
        if(fakeRandom instanceof MockRandomGenerator) {
            ((MockRandomGenerator) fakeRandom).set_fakeMove(App.play.PAPER);
        }
        PlayersMoves moves = remoteMode.getPlayersMoves();

        assert(moves.firstPlayer.equals(App.play.PAPER) && moves.secondPlayer.equals(App.play.PAPER));
    }

    @Test
    public void checkGeneratedMovesAreScissor(){
        if(fakeRandom instanceof MockRandomGenerator) {
            ((MockRandomGenerator) fakeRandom).set_fakeMove(App.play.SCISSORS);
        }
        PlayersMoves moves = remoteMode.getPlayersMoves();
        assert(moves.firstPlayer.equals(App.play.SCISSORS) && moves.secondPlayer.equals(App.play.SCISSORS));
    }
}


