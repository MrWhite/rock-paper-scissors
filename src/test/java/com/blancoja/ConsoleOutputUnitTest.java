package com.blancoja;

import org.junit.Assert;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

public class ConsoleOutputUnitTest {

    private final ByteArrayOutputStream outputStreamCaptor = new ByteArrayOutputStream();
    private final PrintStream standardOut = System.out;

    @BeforeEach
    public void setUp() {
        System.setOut(new PrintStream(outputStreamCaptor));
    }

    @AfterEach
    public void tearDown() {
        System.setOut(standardOut);
    }

    @Test
    void checkMessageConsoleOutputSelection() {
        ConsoleOutput output = new ConsoleOutput();
        output.startNewGame();

        Assertions.assertEquals("-----------------------------------------------------------", outputStreamCaptor.toString().trim());
    }

    @Test
    void checkMessageConsoleOutputRoundPlayed() {
        ConsoleOutput output = new ConsoleOutput();
        Result roundResult = new Result();
        roundResult.value= "FIRST";
        PlayersMoves moves = new PlayersMoves();
        moves.firstPlayer= App.play.PAPER;
        moves.secondPlayer= App.play.ROCK;
        output.publishRoundResults(moves, roundResult, 5);

        Assertions.assertEquals("1st Player Choice:" + moves.firstPlayer + "\n2nd Player Choice:" + moves.secondPlayer + "\nWinner: " + roundResult.value , outputStreamCaptor.toString().trim());
    }

    @Test
    void checkMessageConsoleOutputFinalResults() {
        ConsoleOutput output = new ConsoleOutput();
        TotalResults totalResults = new TotalResults();
        totalResults.actualGameRound= 10;
        totalResults.firstPlayer=7;
        totalResults.secondPlayer=3;
        output.publishFinalResults(totalResults);

        System.out.println("-----------------");
        Assertions.assertEquals("-----------------------------------------------------------\n" +
                "-----------------------------------------------------------\n" +
                "Final Score: \n" +
                "-----------------\n" +
                "1st Player Score: " + totalResults.firstPlayer + "\n" +
                "2nd Player Score: " + totalResults.secondPlayer + "\n" +
                "-----------------------------------------------------------\n" +
                "-----------------------------------------------------------\n" +
                "-----------------" , outputStreamCaptor.toString().trim());
    }
}
