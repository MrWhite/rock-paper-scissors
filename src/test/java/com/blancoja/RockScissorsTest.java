package com.blancoja;

import static org.junit.Assert.assertTrue;

import org.junit.Assert;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;


public class RockScissorsTest
{
    Game rockScissor = new RockPaperScissorsGame(10);


    @Test
    public void firstPlayerWinsTest()
    {
        PlayersMoves moves = new PlayersMoves();
        moves.firstPlayer= App.play.PAPER;
        moves.secondPlayer= App.play.ROCK;
        Result result = rockScissor.playRound(moves);
        Assertions.assertEquals(result.value, "1st Player");
    }

    @Test
    public void secondPlayerWinsTest()
    {
        PlayersMoves moves = new PlayersMoves();
        moves.firstPlayer= App.play.PAPER;
        moves.secondPlayer= App.play.SCISSORS;
        Result result = rockScissor.playRound(moves);
        Assertions.assertEquals(result.value, "2nd Player");
    }

    @Test
    public void drawTest()
    {
        PlayersMoves moves = new PlayersMoves();
        moves.firstPlayer= App.play.PAPER;
        moves.secondPlayer= App.play.PAPER;
        Result result = rockScissor.playRound(moves);
        Assertions.assertEquals(result.value, "Draw");
    }
}
