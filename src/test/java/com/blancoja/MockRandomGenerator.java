package com.blancoja;

public class MockRandomGenerator implements IRandomMoveGenerator{

    private App.play _fakeMove;

    public MockRandomGenerator(App.play fakeMove) {
        _fakeMove=fakeMove;
    }

    public void set_fakeMove(App.play fakeMove) {
        this._fakeMove = fakeMove;
    }

    @Override
    public App.play GetRandomMove() {
        return _fakeMove;
    }
}
