package com.blancoja;

import org.junit.Assert;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.IOException;
import java.io.PrintStream;
import java.nio.charset.StandardCharsets;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;

public class FileOutputUnitTest {
    FileOutput fileOutput;
    @BeforeEach
    public void setUp() {
        fileOutput = new FileOutput("testingfile.txt");

    }

    @AfterEach
    public void tearDown() {
        try {
            Path path = FileSystems.getDefault().getPath("./testingfile.txt");
            Files.deleteIfExists(path);
        } catch(IOException e) {
            System.out.println("Could not delete the file");
        }
    }

    @Test
    void checkMessageFileOutputSelection() {
        fileOutput.startNewGame();
        String response = returnContentsOfFile("testingfile.txt");


        Assertions.assertEquals(" ROCK, PAPER & SCISSORS GAME \n"+
        "-----------------------------\n", response);
    }

    @Test
    void checkMessageFileOutputRoundPlayed() {
        Result roundResult = new Result();
        roundResult.value= "FIRST";
        PlayersMoves moves = new PlayersMoves();
        moves.firstPlayer= App.play.PAPER;
        moves.secondPlayer= App.play.ROCK;
        fileOutput.publishRoundResults(moves, roundResult, 5);

        String response = returnContentsOfFile("testingfile.txt");


        Assertions.assertEquals("Round 5: " +
                "\n-----------------" +
                "\n1st Player Choice:" + moves.firstPlayer +
                "\n2nd Player Choice:" + moves.secondPlayer +
                "\nWinner: " + roundResult.value +
                "\n-----------------------------------------------------------\n", response);
    }

    @Test
    void checkMessageFileOutputFinalResults() {
        TotalResults totalResults = new TotalResults();
        totalResults.actualGameRound= 10;
        totalResults.firstPlayer=7;
        totalResults.secondPlayer=3;
        fileOutput.publishFinalResults(totalResults);

        String response = returnContentsOfFile("testingfile.txt");

        Assertions.assertEquals("-----------------------------------------------------------\n" +
                "-----------------------------------------------------------\n" +
                "Final Score: \n" +
                "-----------------\n" +
                "1st Player Score: " + totalResults.firstPlayer + "\n" +
                "2nd Player Score: " + totalResults.secondPlayer + "\n" +
                "-----------------------------------------------------------\n" +
                "-----------------------------------------------------------\n" , response);
    }

    public String returnContentsOfFile(String fileName) {
        Path path = FileSystems.getDefault().getPath("./" + fileName);
        String response="";
        try {
            byte[] encoded = Files.readAllBytes(path);
            response= new String(encoded, StandardCharsets.UTF_8);
        } catch (Exception e) {
            System.out.println("could not read file");
        }
        return response;
    }
}
